﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Week_1.Models;

namespace Week_1.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            int hour = DateTime.Now.Hour;
            string greeting = hour < 12 ? "Good Morning" : "Good Afternoon";
            greeting += " the curent time is " + DateTime.Now.ToShortTimeString();

            ViewBag.Greeting = greeting;


            return View();
        }

        [HttpGet]
        public ViewResult RsvpForm()
        {       
            return View();
        }

        [HttpPost]
        public ViewResult RsvpForm(GuestResponse guestResponse)
        {
            if (ModelState.IsValid)
            {
                Repository.AddResponse(guestResponse);
                return View("Thanks", guestResponse);
            }
            else
            {
                // post back with the validation error
                return View();
            }
        }

        public ViewResult ListResponses()
        {
            return View(Repository.Responses.Where(r => r.WillAttend == true));
        }
    }
}
